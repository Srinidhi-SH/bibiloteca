package bibiloteca;

import readerAndWriter.ConsoleReader;

// Takes selection from the user and help user to interact with Library

public class BibilotecaConsoleApplication {

    public static void main(String[] args) {
        ConsoleReader consoleReader = new ConsoleReader();
        MainMenu mainMenu = new MainMenu();

        mainMenu.displayOptions();
        while (true) {
            mainMenu.executeSelectedOption(consoleReader.getOption());
            mainMenu.displayOptions();
        }
    }
}
