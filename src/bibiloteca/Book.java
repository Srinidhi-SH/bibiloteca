package bibiloteca;

import types.LibraryItemType;

import static types.LibraryItemType.*;

// All the properties associated with the book

public class Book implements LibraryItem {

    private final int ID;
    private final String name;
    private final String author;
    private final int year;

    public Book(int ID, String name, String author, int year) {
        this.ID = ID;
        this.name = name;
        this.author = author;
        this.year = year;
    }

    public boolean withID(int bookID) {
        return this.ID == bookID;
    }

    @Override
    public String toString() {
        return ID + "  " + name + "  " + author + "  " + year;
    }

    public boolean isOfType(LibraryItemType type) {
        return type == book;
    }

}
