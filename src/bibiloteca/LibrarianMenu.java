package bibiloteca;

import readerAndWriter.ConsoleReader;
import readerAndWriter.ConsoleWriter;

import java.util.HashMap;

import static bibiloteca.MenuItems.*;
import static bibiloteca.MessagesToConsole.INVALID_OPTION;
import static bibiloteca.MessagesToConsole.LIBRARIAN_MENU;
import static bibiloteca.MessagesToConsole.MAIN_MENU;

// Librarian selects the drives through this menu

public class LibrarianMenu {
    private HashMap optionsMap = new HashMap();
    private ConsoleWriter consoleWriter = new ConsoleWriter();
    private ConsoleReader consoleReader = new ConsoleReader();
    private User user;

    LibrarianMenu(User user) {
        this.user = user;
        optionsMap.put(1, CHECKED_OUT_BOOKS);
    }

    public void displayOptions() {
        consoleWriter.printMessage(LIBRARIAN_MENU);
    }


    public String executeSelectedOption(int optionSelected, Library library) {
        if (optionSelected == 2) return null;
        if (!optionsMap.containsKey(optionSelected)) {
            return INVALID_OPTION;
        }
        MenuItems menuItem = (MenuItems) optionsMap.get(optionSelected);
        return (menuItem.execute(library, consoleReader, user));
    }
}
