package bibiloteca;

import repository.Repository;
import types.LibraryItemType;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

// A Library that helps to list , checkout , return - Movie or Books

public class Library {

    private LinkedHashMap libraryItems;

    public Library() {
        libraryItems = new Repository().getLibraryItems();
    }

    public String welcomeToBibiloteca() {
        return "Welcome to Library";
    }

    public String displayableListOf(LibraryItemType type, boolean withUsers) {
        String messageToConsole = "";
        Iterator iterator = libraryItems.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();
            LibraryItem libraryItem = (LibraryItem) pair.getKey();
            User user = (User) pair.getValue();
            if (libraryItem.isOfType(type) && user == null && withUsers == false)
                messageToConsole += libraryItem.toString() + "\n";
            if (libraryItem.isOfType(type) && user != null && withUsers == true)
                messageToConsole += libraryItem.toString() + "\t" + user.associateUserToALibraryItem() + "\n";
        }
        return messageToConsole;
    }

    public LibraryItem checkOutItem(int ID, LibraryItemType type, User user) {
        LibraryItem item = getItem(ID, type);
        if (item == null) return null;
        if (libraryItems.get(item) != null) return null;
        libraryItems.remove(item);
        libraryItems.put(item, user);
        return item;
    }

    public boolean returnItem(int ID, LibraryItemType type, User user) {
        LibraryItem item = getItem(ID, type);
        if (item == null) return false;
        if (libraryItems.get(item) == null) return false;
        if (!libraryItems.get(item).equals(user)) {
            return false;
        }
        libraryItems.remove(item);
        libraryItems.put(item, null);
        return true;
    }

    private LibraryItem getItem(int ID, LibraryItemType type) {
        Iterator iterator = libraryItems.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();
            LibraryItem libraryItem = (LibraryItem) pair.getKey();
            if (libraryItem.withID(ID) && libraryItem.isOfType(type))
                return libraryItem;
        }
        return null;
    }
}

