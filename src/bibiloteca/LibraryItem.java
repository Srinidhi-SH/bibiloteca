package bibiloteca;

import types.LibraryItemType;

// A Book or a Movie

public interface LibraryItem {
  String toString();
  boolean isOfType(LibraryItemType type);
  boolean withID(int ID);
}
