package bibiloteca;

import readerAndWriter.ConsoleReader;
import readerAndWriter.ConsoleWriter;
import repository.Repository;

import java.util.HashMap;


import static bibiloteca.MessagesToConsole.*;
import static bibiloteca.MenuItems.*;
import static types.UserType.*;

// Make operations on Library based on user selected options

public class MainMenu {

    private Library library = new Library();
    private HashMap optionsMap = new HashMap();
    private ConsoleWriter consoleWriter = new ConsoleWriter();
    private ConsoleReader consoleReader = new ConsoleReader();
    private Repository repository = new Repository();

    public MainMenu() {
        System.out.println(library.welcomeToBibiloteca());
        optionsMap.put(1, LIST_BOOKS);
        optionsMap.put(2, LIST_MOVIES);
        optionsMap.put(3, null);
        optionsMap.put(4, QUIT_CONSOLE);
    }

    public void executeSelectedOption(int optionSelected) {
        if (optionSelected != 3) {
            MenuItems menuItem = (MenuItems) optionsMap.get(optionSelected);
            consoleWriter.printMessage(menuItem.execute(library, consoleReader, null));
            return;
        }
        if (!optionsMap.containsKey(optionSelected)) {
            consoleWriter.printMessage(INVALID_OPTION);
            return;
        }
        User user = authenticateUser();
        if (user != null) {
            if (user.isOfType(GENERAL_USER)) {
                driveUserMenu(user);
                return;
            }
            driveLibrarianMenu(user);
            return;
        }
        consoleWriter.printMessage(INVALID_CREDENTIALS);
    }


    public void displayOptions() {
        consoleWriter.printMessage(MAIN_MENU);
    }

    private void driveLibrarianMenu(User user) {
        LibrarianMenu librarianMenu = new LibrarianMenu(user);
        librarianMenu.displayOptions();
        int optionSelected = consoleReader.getOption();
        while (optionSelected != 2) {
            consoleWriter.printMessage(librarianMenu.executeSelectedOption(optionSelected, library));
            librarianMenu.displayOptions();
            optionSelected = consoleReader.getOption();
        }
    }

    private void driveUserMenu(User user) {
        UserMenu userMenu = new UserMenu(user);
        userMenu.displayOptions();
        int optionSelected = consoleReader.getOption();
        while (optionSelected != 8) {
            consoleWriter.printMessage(userMenu.executeSelectedOption(optionSelected, library));
            userMenu.displayOptions();
            optionSelected = consoleReader.getOption();
        }
    }

    private User authenticateUser() {
        String nameAndPassword[] = consoleReader.getUserNameAndPassword();
        for (User user : repository.getUsers()) {
            if (user.authenticate(nameAndPassword[0], nameAndPassword[1]))
                return user;
        }
        return null;
    }
}
