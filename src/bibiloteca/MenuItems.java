package bibiloteca;

import readerAndWriter.Reader;

import static bibiloteca.MessagesToConsole.*;
import static types.LibraryItemType.*;

//Returns Message based on an operation

public interface MenuItems {
    ListBooks LIST_BOOKS = new ListBooks();
    CheckOutBook CHECK_OUT_BOOK = new CheckOutBook();
    ReturnBook RETURN_BOOK = new ReturnBook();
    ListMovies LIST_MOVIES = new ListMovies();
    CheckOutMovie CHECK_OUT_MOVIE = new CheckOutMovie();
    ReturnMovie RETURN_MOVIE = new ReturnMovie();
    Quit QUIT_CONSOLE = new Quit();
    UserInformation USER_INFORMATION = new UserInformation();
    CheckedOutBooks CHECKED_OUT_BOOKS = new CheckedOutBooks();

    String execute(Library library, Reader reader, User user);
}

class CheckedOutBooks implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return CHECKED_OUT_COLUMNS+(library.displayableListOf(book, true));
    }
}

class UserInformation implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return (user.toString() + "\n");
    }
}

class ListMovies implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return (MOVIE_COLUMNS+library.displayableListOf(movie, false));
    }
}

class ListBooks implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return (BOOK_COLUMNS+library.displayableListOf(book, false));
    }
}

class CheckOutMovie implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return library.checkOutItem(reader.getID(), movie, user) != null ? SUCCESSFUL_CHECK_OUT_MOVIE : UNSUCCESSFUL_CHECK_OUT_MOVIE;
    }
}

class ReturnMovie implements MenuItems {

    @Override
    public String execute(Library library, Reader reader, User user) {
        return library.returnItem(reader.getID(), movie, user) ? SUCCESSFUL_RETURN_MOVIE : UNSUCCESSFUL_RETURN_MOVIE;
    }
}

class CheckOutBook implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return library.checkOutItem(reader.getID(), book, user) != null ? SUCCESSFUL_CHECK_OUT : UNSUCCESSFUL_CHECK_OUT;
    }
}

class ReturnBook implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        return library.returnItem(reader.getID(), book, user) ? SUCCESSFUL_RETURN : UNSUCCESSFUL_RETURN;
    }
}

class Quit implements MenuItems {
    @Override
    public String execute(Library library, Reader reader, User user) {
        System.exit(0);
        return null;
    }
}


