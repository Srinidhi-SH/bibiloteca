package bibiloteca;

// Has all the Strings returned on executing an operation

public interface MessagesToConsole {

    String BOOK_COLUMNS = "\nID\tName\tAuthor\tYear\n--------------------------\n";
    String MOVIE_COLUMNS = "\nID\tName\tYear\tDirector\tRating\n------------------------------------------\n";
    String CHECKED_OUT_COLUMNS = "\nID\tName\tAuthor\tYear\tLibrary Number\n--------------------------------------------\n";
    String LIBRARIAN_MENU = "LIBRARIAN MENU\n\n1.List Checked Out Books\n2.Log out";
    String MAIN_MENU = "MAIN MENU\n\n1.List Books\n2.List Movies\n3.Log In\n4.Quit";
    String USER_MENU = "USER MENU\n\n1.List Books\n2.List Movies\n3.Check Out Book\n4.Return Book\n5.Check Out Movie\n6.Return Movie\n7.User Details\n8.Log Out";
    String SUCCESSFUL_RETURN = "Thank you for returning the book\n";
    String SUCCESSFUL_CHECK_OUT_MOVIE = "Enjoy The Movie !\n";
    String UNSUCCESSFUL_CHECK_OUT_MOVIE = "Movie is not available\n";
    String UNSUCCESSFUL_RETURN = "That is not a valid book to return\n";
    String SUCCESSFUL_CHECK_OUT = "Thank you! Enjoy the book\n";
    String UNSUCCESSFUL_CHECK_OUT = "Book is not available\n";
    String INVALID_OPTION = "Select a valid option!";
    String INVALID_CREDENTIALS = "Invalid Credentials!Please Login Again\n";
    String SUCCESSFUL_RETURN_MOVIE = "Thank you for returning the movie\n";
    String UNSUCCESSFUL_RETURN_MOVIE = "This is not a valid movie to return\n";
}
