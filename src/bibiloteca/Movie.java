package bibiloteca;

import types.LibraryItemType;

import static types.LibraryItemType.*;

// Has all the properties associated with a Movie

public class Movie implements LibraryItem {
    private final int ID, year;
    private final String name, director, rating;

    public Movie(int ID, String name, int year, String director, String rating) {
        this.ID = ID;
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }

    public boolean withID(int ID) {
        return this.ID == ID;
    }

    @Override
    public String toString() {
        return ID + "  " + name + "  " + "  " + year + "  " + director + "  " + rating;
    }

    public boolean isOfType(LibraryItemType type) {
        return type == movie;
    }
}
