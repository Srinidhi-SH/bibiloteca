package bibiloteca;

import types.UserType;

// Has all the properties associated with a user

public class User {
    private String libraryNumber;
    private String password;
    private String name, email, address;
    private int phoneNumber;
    private UserType userType;

    public User(String libraryNumber, String password, String name, String email, String address, int phoneNumber, UserType userType) {
        this.libraryNumber = libraryNumber;
        this.password = password;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.userType = userType;
    }

    public String associateUserToALibraryItem() {
        return libraryNumber;
    }

    public boolean authenticate(String libraryNumber, String password) {
        return this.libraryNumber.equals(libraryNumber) && this.password.equals(password);
    }

    @Override
    public String toString() {
        return "Name:" + name + "\nemail:" + email + "\nAddress:" + address + "\nContact:" + phoneNumber;
    }

    public boolean isOfType(UserType userType) {
        return this.userType == userType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (phoneNumber != user.phoneNumber) return false;
        if (libraryNumber != null ? !libraryNumber.equals(user.libraryNumber) : user.libraryNumber != null)
            return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return address != null ? address.equals(user.address) : user.address == null;

    }

    @Override
    public int hashCode() {
        int result = libraryNumber != null ? libraryNumber.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + phoneNumber;
        return result;
    }
}
