package bibiloteca;

import readerAndWriter.ConsoleReader;
import readerAndWriter.ConsoleWriter;

import java.util.HashMap;

import static bibiloteca.MenuItems.*;
import static bibiloteca.MessagesToConsole.INVALID_OPTION;
import static bibiloteca.MessagesToConsole.USER_MENU;

// User drives through the menu and executes an operation

public class UserMenu {
    private HashMap optionsMap = new HashMap();
    private final User user;
    private ConsoleWriter consoleWriter = new ConsoleWriter();
    private ConsoleReader consoleReader = new ConsoleReader();

    UserMenu(User user) {
        this.user = user;
        optionsMap.put(1, LIST_BOOKS);
        optionsMap.put(2, LIST_MOVIES);
        optionsMap.put(3, CHECK_OUT_BOOK);
        optionsMap.put(4, RETURN_BOOK);
        optionsMap.put(5, CHECK_OUT_MOVIE);
        optionsMap.put(6, RETURN_MOVIE);
        optionsMap.put(7, USER_INFORMATION);
    }

    public void displayOptions() {
        consoleWriter.printMessage(USER_MENU);
    }


    public String executeSelectedOption(int optionSelected, Library library) {
        if (optionSelected == 8) return null;
        if (!optionsMap.containsKey(optionSelected)) {
            return INVALID_OPTION;
        }
        MenuItems menuItem = (MenuItems) optionsMap.get(optionSelected);
        return (menuItem.execute(library, consoleReader, user));
    }
}
