package readerAndWriter;

import java.util.Scanner;

/**
 * Gets the inputs from the console
 */

public class ConsoleReader implements Reader {
    private final Scanner scanner = new Scanner(System.in);

    public int getID() {
        System.out.println("Enter ID :");
        return Integer.parseInt(scanner.nextLine());
    }

    public int getOption() {
        return Integer.parseInt(scanner.nextLine());
    }

    public String[] getUserNameAndPassword() {
        String userNameAndPassword[] = new String[2];
        System.out.println("Enter UserName :");
        userNameAndPassword[0] = scanner.nextLine();
        System.out.println("Enter Password :");
        userNameAndPassword[1] = scanner.nextLine();
        return userNameAndPassword;
    }
}
