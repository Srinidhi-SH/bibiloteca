package readerAndWriter;

/**
 * Writes the string passed into the Console
 */
public class ConsoleWriter implements Writer{

  public void printMessage(String message){
    System.out.println(message);
  }
}
