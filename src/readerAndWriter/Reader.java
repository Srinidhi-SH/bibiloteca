package readerAndWriter;

public interface Reader {
  int getID();
  int getOption();
  String[] getUserNameAndPassword();
}
