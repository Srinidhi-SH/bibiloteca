package readerAndWriter;

public interface Writer {
  void printMessage(String message);
}
