package repository;

import bibiloteca.Book;
import bibiloteca.Movie;
import bibiloteca.User;
import types.UserType;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Information is taken from the database
 */

public class Repository {
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public LinkedHashMap getLibraryItems() {
        LinkedHashMap libraryItems = new LinkedHashMap();
        ArrayList<Book> books = getBooks();
        ArrayList<Movie> movies = getMovies();
        for (Movie movie : movies)
            libraryItems.put(movie, null);
        for (Book book : books)
            libraryItems.put(book, null);
        return libraryItems;
    }

    public ArrayList<Book> getBooks() {
        ArrayList<Book> books = new ArrayList<>();
        try {
            resultSet = openDataBaseAndExecuteQuery("SELECT ID,NAME,AUTHOR,YEAR FROM BOOK");
            while (resultSet.next()) {
                books.add(new Book(resultSet.getInt("ID"), resultSet.getString("NAME"), resultSet.getString("AUTHOR"), resultSet.getInt("YEAR")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                closeDataBaseConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return books;
    }

    public ArrayList<Movie> getMovies() {
        ArrayList<Movie> movies = new ArrayList<>();
        try {
            resultSet = openDataBaseAndExecuteQuery("SELECT ID,NAME,YEAR,DIRECTOR,RATING FROM MOVIE");
            while (resultSet.next()) {
                movies.add(new Movie(resultSet.getInt("ID"), resultSet.getString("NAME"), resultSet.getInt("YEAR"), resultSet.getString("DIRECTOR"), resultSet.getString("RATING")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                closeDataBaseConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return movies;
    }

    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        try {
            resultSet = openDataBaseAndExecuteQuery("SELECT LIBRARYNUMBER,PASSWORD,NAME,EMAIL,ADDRESS,CONTACT,TYPE FROM USER");
            while (resultSet.next()) {
                users.add(new User(resultSet.getString("LIBRARYNUMBER"), resultSet.getString("PASSWORD"), resultSet.getString("NAME"), resultSet.getString("EMAIL"), resultSet.getString("ADDRESS"), resultSet.getInt("CONTACT"), UserType.valueOf(resultSet.getString("TYPE").toUpperCase().replace(" ", "_"))));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                closeDataBaseConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return users;
    }

    private ResultSet openDataBaseAndExecuteQuery(String Query) throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:/Users/hsrinidhi/bibiloteca/bibiloteca.sqlite");
        statement = connection.createStatement();
        return statement.executeQuery(Query);
    }

    private void closeDataBaseConnection() throws SQLException {
        resultSet.close();
        statement.close();
        connection.close();
    }
}
