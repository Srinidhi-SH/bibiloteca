package types;

// Represents the library items

public enum LibraryItemType {
    book("book"), movie("movie");

    private String stringType;

    LibraryItemType(String stringType) {

        this.stringType = stringType;
    }

    public String getStringType() {
        return stringType;
    }
}
