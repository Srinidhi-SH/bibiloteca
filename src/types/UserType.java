package types;

// There can be two types of Users

public enum UserType {
    GENERAL_USER, LIBRARIAN
}
