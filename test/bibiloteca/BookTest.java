package bibiloteca;

import org.junit.Assert;
import org.junit.Test;
import types.LibraryItemType;

import static org.junit.Assert.*;

public class BookTest {

  @Test
  public void createABookAndDisplayingItSoAsToPrintThatInAList() {

    assertEquals("1  Book 1  Author 1  1999", new Book(1, "Book 1", "Author 1", 1999).toString());
  }

  @Test
  public void identifyABookWhichHasSpecificID() {

    assertTrue(new Book(1, "Book 1", "Author 1", 1900).withID(1));
  }

  @Test
  public void checkingTheTypeOfBook(){

    assertTrue(new Book(1,"Book1","Author1",1900).isOfType(LibraryItemType.book));
  }
}
