package bibiloteca;

import org.junit.Assert;
import org.junit.Test;
import types.LibraryItemType;
import types.UserType;

import static bibiloteca.MessagesToConsole.*;
import static org.junit.Assert.*;

public class LibrarianMenuTest {

  @Test
  public void librarianMustBeAbleToSeeCheckedOutBooksOnOptionOne(){
    Library library = library();
    library.checkOutItem(1, LibraryItemType.book,user());
    assertEquals("1  Book 1  Author 1  2000\t234-5678\n",new LibrarianMenu(user()).executeSelectedOption(1,library));
  }

  @Test
  public void librarianMustBeAbleToLogOutOnOptionTwo(){
    assertEquals(null,new LibrarianMenu(user()).executeSelectedOption(2,library()));
  }

  @Test
  public void invalidOptionOnSelectingOptionThree(){
    assertEquals(INVALID_OPTION,new LibrarianMenu(user()).executeSelectedOption(3,library()));
  }

  private User user(){
    return new User("234-5678","password2","User 2","user2@email.com","Address 2",12345, UserType.GENERAL_USER);
  }

  private Library library(){
    return new Library();
  }
}
