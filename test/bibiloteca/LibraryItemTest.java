package bibiloteca;

import org.junit.Assert;
import org.junit.Test;
import types.LibraryItemType;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static types.LibraryItemType.*;

public class LibraryItemTest {

  @Test
  public void createBookAsALibraryItemAndGetItsType(){

    LibraryItem libraryItem = mock(LibraryItem.class);
    when(libraryItem.isOfType(book)).thenReturn(true);
  }

  @Test
  public void createAMovieAndIdentifyByItsID(){

    LibraryItem libraryItem = mock(LibraryItem.class);
    when(libraryItem.withID(1)).thenReturn(true);
  }

  @Test
  public void createABookAsALibraryItemAndToStringTheBook(){

    LibraryItem libraryItem = mock(LibraryItem.class);
    when(libraryItem.toString()).thenReturn("1 Book1 Author1 1900");
  }
}
