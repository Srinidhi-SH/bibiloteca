package bibiloteca;

import org.junit.Test;
import repository.Repository;
import types.LibraryItemType;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static types.LibraryItemType.*;
import static types.UserType.*;


public class LibraryTest {

    @Test
    public void shouldBeAbleToSeeTheListOfBooks() {
        assertEquals(listOfBook(new Repository().getBooks()), new Library().displayableListOf(LibraryItemType.valueOf("book"), false));
    }


    @Test
    public void shouldBeAbleToSeeTheListOfMovies() {
        assertEquals(listOfMovies(new Repository().getMovies()), new Library().displayableListOf(LibraryItemType.valueOf("movie"), false));
    }

    @Test
    public void mustBeAbleToCheckOutABook() {
        Library library = new Library();
        assertEquals(new Book(1, "Book 1", "Author 1", 2000).toString(), library.checkOutItem(1, book, user()).toString());
    }

    @Test
    public void shouldNotBeAbleToCheckOutABookWhichIsNotPresentInList() {
        Library library = new Library();
        assertNull(library.checkOutItem(4, book, user()));
    }

    @Test
    public void shouldNotBeAbleToCheckOutABookWhichIsNotAvailable() {
        Library library = new Library();
        library.checkOutItem(1, book, user());
        assertNull(library.checkOutItem(1, book, user()));
    }

    @Test
    public void mustBeAbleToCheckOutAMovie() {
        Library library = new Library();
        assertEquals(new Movie(1, "Movie 1", 1900, "Director 1", "10").toString(), library.checkOutItem(1, movie, user()).toString());
    }

    @Test
    public void mustBeAbleToReturnABook() {
        Library library = new Library();
        library.checkOutItem(1, book, user());
        assertTrue(library.returnItem(1, book, user()));
    }

    @Test
    public void shouldNotBeAbleToReturnABookWhichIsNotInTheList(){
        Library library = new Library();
        assertFalse(library.returnItem(4, book, user()));
    }

    @Test
    public void shouldNotBeAbleToReturnABookWhichIsNotCheckedOut(){
        Library library = new Library();
        assertFalse(library.returnItem(1, book, user()));
    }

    @Test
    public void mustBeAbleToReturnAMovie() {
        Library library = new Library();
        library.checkOutItem(1, movie, user());
        assertTrue(library.returnItem(1, movie, user()));
    }

    @Test
    public void mustBeAbleToSeeWhoCheckedOutABook(){
        Library library = new Library();
        library.checkOutItem(1, book, user());
        assertEquals("1  Book 1  Author 1  2000\t234-5678\n",library.displayableListOf(book,true));
    }

    private User user() {
        return new User("234-5678", "password2", "User 2", "user2@email.com", "Address 2", 123456, GENERAL_USER);
    }

    private ArrayList<Movie> getMovies() {
        return new Repository().getMovies();
    }

    private ArrayList<Book> getBooks() {
        return new Repository().getBooks();
    }

    private String listOfMovies(ArrayList<Movie> movies) {
        String expectedConsoleMessage = "";
        for (Movie movie : movies)
            expectedConsoleMessage += movie.toString() + "\n";
        return expectedConsoleMessage;
    }

    private String listOfBook(ArrayList<Book> books) {
        String expectedConsoleMessage = "";
        for (Book book : books)
            expectedConsoleMessage += book.toString() + "\n";
        return expectedConsoleMessage;
    }
}
