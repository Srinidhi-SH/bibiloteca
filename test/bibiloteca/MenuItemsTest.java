package bibiloteca;

import org.junit.Test;
import readerAndWriter.ConsoleReader;
import readerAndWriter.Reader;
import repository.Repository;
import types.UserType;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static types.LibraryItemType.*;

public class MenuItemsTest {

  @Test
  public void mockTheMenuItemInterFaceAndExpectItToReturnAString(){
    MenuItems menuItems = mock(MenuItems.class);
    when(menuItems.execute(library(),consoleReader(),user())).thenReturn("menu Item executed");
  }

  @Test
  public void userInformationIsDisplayedOnExecutingUserInformation(){
    UserInformation userInformation = new UserInformation();
    assertEquals(user().toString()+"\n",userInformation.execute(library(),consoleReader(),user()));
  }

  @Test
  public void listsTheBookOnExecutingListBooks(){
    ListBooks listBooks = new ListBooks();
    assertEquals(displayableListOfBooks(new Repository().getBooks()),listBooks.execute(library(),consoleReader(),user()));
  }

  @Test
  public void listsTheMoviesOnExecutingListMovies(){
    ListMovies listMovies = new ListMovies();
    assertEquals(displayableListOfMovies(new Repository().getMovies()),listMovies.execute(library(),consoleReader(),user()));
  }

  @Test
  public void librarianMustBeAbleToSeeCheckedOutBooks(){
    Library library = library();
    library.checkOutItem(1, book,user());
    assertEquals("1  Book 1  Author 1  2000\t234-5678\n",new CheckedOutBooks().execute(library,consoleReader(),user()));
  }

  // Test for check out movie and book , return Movie and Book

  private String displayableListOfMovies(ArrayList<Movie> movies) {
    String expectedConsoleMessage = "";
    for (Movie movie : movies)
      expectedConsoleMessage += movie.toString() + "\n";
    return expectedConsoleMessage;
  }

  private String displayableListOfBooks(ArrayList<Book> books) {
    String expectedConsoleMessage = "";
    for (Book book : books)
      expectedConsoleMessage += book.toString() + "\n";
    return expectedConsoleMessage;
  }

  private Library library(){
    return new Library();
  }

  private User user(){
    return new User("234-5678","password2","User 2","user2@email.com","Address 2",12345, UserType.GENERAL_USER);
  }
  private Reader consoleReader(){
    return new ConsoleReader();
  }
}
