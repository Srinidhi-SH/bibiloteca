package bibiloteca;

import org.junit.Test;

import static org.junit.Assert.*;
import static types.LibraryItemType.*;

public class MovieTest {

  @Test
  public void checkToSeeIfAMovieMatchesWithThisID(){

    assertEquals("1  Movie 1    1990  Director 1  8",new Movie(1,"Movie 1",1990,"Director 1","8").toString());
  }

  @Test
  public void checkToSeeIfAMovieDoesNotMatchesWithThisID(){

    assertFalse(new Movie(1,"Movie 1",1990,"Director 1","8").withID(2));
  }

  @Test
  public void movieIsOfTypeMovie(){

    assertTrue(new Movie(1,"Movie 1",1990,"Director 1","8").isOfType(movie));
  }
}
