package bibiloteca;

import org.junit.Test;
import readerAndWriter.Reader;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReaderTest {

  @Test
  public void returnAnIDOnCallingGetID(){
    Reader reader=mock(Reader.class);
    when(reader.getID()).thenReturn(1);
  }

  @Test
  public void returnAnOptionOnGetOption() {
    Reader reader = mock(Reader.class);
    when(reader.getOption()).thenReturn(1);
  }

  @Test
  public void returnStringArrayOnGetUserNameAndPassword(){
    Reader reader = mock(Reader.class);
    when(reader.getUserNameAndPassword()).thenReturn(new String[]{"123-5678", "password1"});
  }
}
