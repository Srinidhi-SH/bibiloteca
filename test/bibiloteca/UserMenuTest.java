package bibiloteca;

import org.junit.Test;
import repository.Repository;
import types.UserType;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserMenuTest {

  @Test
  public void mustBeAbleToSeeListOfMoviesOnSelectingListOfMovies(){
    assertEquals(displayableListOfMovies(new Repository().getMovies()),userMenu().executeSelectedOption(2,library()));
  }

  @Test
  public void mustBeAbleToSeeListOfBooksOnSelectingListOfBooks(){
    assertEquals(displayableListOfBooks(new Repository().getBooks()),userMenu().executeSelectedOption(1,library()));
  }

  @Test
  public void returnNullOnSelectingLogOut(){
    assertEquals(null,userMenu().executeSelectedOption(8,library()));
  }

  @Test
  public void userMustBeAbleToSeeDetailsOnUserDetails(){
    assertEquals(user().toString()+"\n",userMenu().executeSelectedOption(7,library()));
  }

  // Check out and return of Book and Movie

  private UserMenu userMenu(){
    return new UserMenu(user());
  }

  private Library library(){
    return new Library();
  }
  private User user(){
    return new User("234-5678","password2","User 2","user2@email.com","Address 2",12345, UserType.GENERAL_USER);
  }

  private String displayableListOfMovies(ArrayList<Movie> movies) {
    String expectedConsoleMessage = "";
    for (Movie movie : movies)
      expectedConsoleMessage += movie.toString() + "\n";
    return expectedConsoleMessage;
  }

  private String displayableListOfBooks(ArrayList<Book> books) {
    String expectedConsoleMessage = "";
    for (Book book : books)
      expectedConsoleMessage += book.toString() + "\n";
    return expectedConsoleMessage;
  }
}
