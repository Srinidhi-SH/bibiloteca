package bibiloteca;

import org.junit.Test;

import static org.junit.Assert.*;
import static types.UserType.*;

public class UserTest {

  @Test
  public void createAUserAndValidateHimWithCredentials(){
    assertTrue(user().authenticate("123-4567","password1"));
  }

  @Test
  public void displayTheDetailsOfTheUser(){
    assertEquals("Name:User1\nemail:user@user.com\nAddress:Address1\nContact:12345",user().toString());
  }

  @Test
  public void userAssociatedAsGeneralUSer(){
    assertTrue(user().isOfType(GENERAL_USER));
  }

  @Test
  public void getTheLibraryNumberOnAssociateUserWithAnItem(){
    assertEquals("123-4567",user().associateUserToALibraryItem());
  }

  @Test
  public void compareEqualityInUsers(){
    assertEquals(user(),user());
  }

  private User user(){
    return new User("123-4567","password1","User1","user@user.com","Address1",12345, GENERAL_USER);
  }
}
